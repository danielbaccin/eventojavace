package br.com.kenuiapps.eventojavace;

import android.test.AndroidTestCase;

import java.util.Date;

import br.com.kenuiapps.eventojavace.dao.EventoDAO;
import br.com.kenuiapps.eventojavace.model.Evento;

/**
 * Created by DanielBaccin on 17/05/15.
 */
public class EventoDAOTest extends AndroidTestCase {

    public void testInsertDb()
    {
        EventoDAO dao = new EventoDAO(mContext);
        long qtdDeLinhasInseridas = dao.inserir(dadoUmEvento());
        assertTrue(qtdDeLinhasInseridas>0);
    }

    private Evento dadoUmEvento() {
        Evento evento = new Evento();
        evento.setIdServidor(Long.valueOf(100));
        evento.setNome("Primeiro evento");
        evento.setDataInicio(new Date());
        evento.setDataFim(new Date());
        evento.setInformacoes("evento teste");
        evento.setFeedBack("60 likes");
        return evento;
    }

}
