package br.com.kenuiapps.eventojavace;

import android.test.AndroidTestCase;

import br.com.kenuiapps.eventojavace.dao.ParticipanteDAO;
import br.com.kenuiapps.eventojavace.model.Participante;

/**
 * Created by danielbaccin on 18/05/15.
 */
public class ParticipanteDAOTest extends AndroidTestCase {

    public void testInsertDb()
    {
        ParticipanteDAO dao = new ParticipanteDAO(mContext);
        long qtdDeLinhasInseridas = dao.inserir(dadoUmParticipante());
        assertTrue(qtdDeLinhasInseridas>0);
    }

    private Participante dadoUmParticipante() {
        Participante participante = new Participante();
        participante.setIdNoServidor(Long.valueOf(100));
        participante.setEventoId(Long.valueOf(1));
        participante.setNome("Daniel Baccin");
        participante.setCpf("00787805351");
        return participante;
    }

}
