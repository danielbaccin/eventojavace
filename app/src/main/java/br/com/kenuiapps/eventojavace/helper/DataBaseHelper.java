package br.com.kenuiapps.eventojavace.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by DanielBaccin on 17/05/15.
 */
public class DataBaseHelper extends SQLiteOpenHelper{

    private static final String BANCO_DADOS = "EventosJavaCE";
    private static int VERSAO = 1;


    public DataBaseHelper(Context context) {
        super(context, BANCO_DADOS, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE evento (" +
                    "_id INTEGER PRIMARY KEY," +
                    " idServidor INTEGER," +
                    " nome TEXT," +
                    " dataInicio DATE," +
                    " dataFim DATE," +
                    " informacoes TEXT," +
                    " feedback TEXT" +
                    " );");
        db.execSQL("CREATE TABLE participante (" +
                    "_id INTEGER PRIMARY KEY," +
                    " idServidor INTEGER," +
                    " idEvento INTEGER," +
                    " nome TEXT," +
                    " cpf TEXT" +
                    " );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public static class Evento{
        public static final String TABELA = "evento";
        public static final String ID = "_id";
        public static final String ID_SERVIDOR = "idServidor";
        public static final String NOME = "nome";
        public static final String DATA_INICIO = "dataInicio";
        public static final String DATA_FIM = "dataFim";
        public static final String INFORMACOES = "informacoes";
        public static final String FEEDBACK = "feedback";
        public static final String[] COLUNAS = new String[]{ID, ID_SERVIDOR, NOME, DATA_INICIO, DATA_FIM, INFORMACOES, FEEDBACK};
    }
    public static class Participante{
        public static final String TABELA = "participante";
        public static final String ID = "_id";
        public static final String ID_SERVIDOR = "idServidor";
        public static final String ID_EVENTO = "idEvento";
        public static final String NOME = "nome";
        public static final String CPF = "cpf";
        public static final String[] COLUNAS = new String[]{ID, ID_SERVIDOR, ID_EVENTO, NOME, CPF};
    }
}
