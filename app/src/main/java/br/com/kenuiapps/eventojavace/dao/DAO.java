package br.com.kenuiapps.eventojavace.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import br.com.kenuiapps.eventojavace.helper.DataBaseHelper;

/**
 * Created by DanielBaccin on 17/05/15.
 */
public class DAO{

    private DataBaseHelper helper;
    private SQLiteDatabase db;

    public DAO(Context context){
        helper = new DataBaseHelper(context);
    }

    public SQLiteDatabase getDb() {
        if (db == null) {
            db = helper.getWritableDatabase();
        }
        return db;
    }

    public void close(){
        helper.close();
    }

}
