package br.com.kenuiapps.eventojavace.model;

import android.content.ContentValues;
import android.database.Cursor;

import br.com.kenuiapps.eventojavace.helper.DataBaseHelper;

/**
 * Created by daniel on 18/05/15.
 */
public class Participante {

    private Long id;
    private Long idNoServidor;
    private Long eventoId;
    private String nome;
    private String cpf;

    public Participante() {}

    public Participante(Cursor cursor) {
        setId(cursor.getLong(cursor.getColumnIndex(DataBaseHelper.Participante.ID)));
        setEventoId(cursor.getLong(cursor.getColumnIndex(DataBaseHelper.Participante.ID_EVENTO)));
        setIdNoServidor(cursor.getLong(cursor.getColumnIndex(DataBaseHelper.Participante.ID_SERVIDOR)));
        setNome(cursor.getString(cursor.getColumnIndex(DataBaseHelper.Participante.NOME)));
        setCpf(cursor.getString(cursor.getColumnIndex(DataBaseHelper.Participante.CPF)));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEventoId() {
        return eventoId;
    }

    public void setEventoId(Long eventoId) {
        this.eventoId = eventoId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Long getIdNoServidor() {
        return idNoServidor;
    }

    public void setIdNoServidor(Long idNoServidor) {
        this.idNoServidor = idNoServidor;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.Participante.ID, getId());
        values.put(DataBaseHelper.Participante.ID_EVENTO, getEventoId());
        values.put(DataBaseHelper.Participante.ID_SERVIDOR, getIdNoServidor());
        values.put(DataBaseHelper.Participante.NOME, getNome());
        values.put(DataBaseHelper.Participante.CPF, getCpf());
        return values;

    }
}
