package br.com.kenuiapps.eventojavace.model;

import android.content.ContentValues;

import java.util.Date;

import br.com.kenuiapps.eventojavace.helper.DataBaseHelper;

/**
 * Created by DanielBaccin on 17/05/15.
 */
public class Evento {

    private Long id;
    private Long idServidor;
    private String nome;
    private Date dataInicio;
    private Date dataFim;
    private String informacoes;
    private String feedBack;


    public Evento(Long id, Long idServidor, String nome, Date dataInicio, Date dataFim, String informacoes, String feedBack) {
        this.id = id;
        this.nome = nome;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.informacoes = informacoes;
        this.feedBack = feedBack;
    }

    public Evento() {
    }

    public Long getIdServidor() {
        return idServidor;
    }

    public void setIdServidor(Long idServidor) {
        this.idServidor = idServidor;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getInformacoes() {
        return informacoes;
    }

    public void setInformacoes(String informacoes) {
        this.informacoes = informacoes;
    }

    public String getFeedBack() {
        return feedBack;
    }

    public void setFeedBack(String feedBack) {
        this.feedBack = feedBack;
    }

    public ContentValues toValues() {
        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.Evento.ID, getId());
        values.put(DataBaseHelper.Evento.ID_SERVIDOR, getIdServidor());
        values.put(DataBaseHelper.Evento.NOME, getNome());
        values.put(DataBaseHelper.Evento.DATA_INICIO, getDataInicio().getTime());
        values.put(DataBaseHelper.Evento.DATA_FIM, getDataFim().getTime());
        values.put(DataBaseHelper.Evento.INFORMACOES, getInformacoes());
        values.put(DataBaseHelper.Evento.FEEDBACK, getFeedBack());

        return values;
    }
}
