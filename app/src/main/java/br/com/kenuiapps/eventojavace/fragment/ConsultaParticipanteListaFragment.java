package br.com.kenuiapps.eventojavace.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import br.com.kenuiapps.eventojavace.R;
import br.com.kenuiapps.eventojavace.adapter.ParticipanteAdapter;
import br.com.kenuiapps.eventojavace.dao.ParticipanteDAO;
import br.com.kenuiapps.eventojavace.model.Constantes;
import br.com.kenuiapps.eventojavace.model.Participante;


/**
 * Created by daniel on 18/05/15.
 */
public class ConsultaParticipanteListaFragment extends Fragment {

    Spinner spinnerTipoBusca;
    EditText editConsultaParticipante;
    ListView listDeParticipantes;
    HashMap<String, String> spinnerMap;
    private AlertDialog dialogConfirmacao;
    private Participante participante;
    private Long eventoId;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_consulta_participante_lista, null);
        findViews(rootView);
        inicializaListiners();

        eventoId = getArguments().getLong(Constantes.EVENTO_ID);
        String[] spinnerArray = new String[]{"CPF", "Nome"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, spinnerArray);
        spinnerMap = new HashMap<String, String>();
        spinnerMap.put("1", "CPF");
        spinnerMap.put("2", "Nome");

        spinnerTipoBusca.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        populaLista();

    }
    private void populaLista() {
        ParticipanteDAO dao = new ParticipanteDAO(getActivity());
        List<Participante> participantes = dao.listaParticipantesDoEvento(eventoId);
        ParticipanteAdapter participanteAdapter = new ParticipanteAdapter(getActivity(), participantes);
        listDeParticipantes.setAdapter(participanteAdapter);
        dao.close();
    }


    private void findViews(View view) {
        spinnerTipoBusca = (Spinner) view.findViewById(R.id.spinnerTipoBusca);
        editConsultaParticipante = (EditText) view.findViewById(R.id.edtConsultaConsumidor);
        listDeParticipantes = (ListView) view.findViewById(R.id.listaParticipantes);
    }

    private void listOnItemClick(final Participante participante, final View view) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.dialog_participante_title));
        dialog.setMessage(getString(R.string.confirmacao_presenca));
        dialog.setNegativeButton(R.string.sim, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ImageView imageView = (ImageView)  view.findViewById(R.id.icon_participante);
                imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_action_accept_black));
                Toast.makeText(getActivity(), "Confirma presença", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.setPositiveButton(R.string.nao, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ImageView imageView = (ImageView)  view.findViewById(R.id.icon_participante);
                imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_no_image));
                Toast.makeText(getActivity(), "Não Confirma presença", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }


    private void inicializaListiners() {
        listDeParticipantes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listOnItemClick((Participante) parent.getItemAtPosition(position), view);
            }
        });
        editConsultaParticipante.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Toast.makeText(getActivity(), "Search...", Toast.LENGTH_SHORT).show();
                    handled = true;
                }
                return handled;
            }
        });
    }
}
