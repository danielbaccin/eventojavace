package br.com.kenuiapps.eventojavace.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import br.com.kenuiapps.eventojavace.R;
import br.com.kenuiapps.eventojavace.model.Evento;

/**
 * Created by daniel on 18/05/15.
 */
public class ListaDeEventosAdapter extends BaseAdapter{

    private Activity activity;
    private List<Evento> eventos;
    private SimpleDateFormat dateFormat;

    public ListaDeEventosAdapter(Activity activity, List<Evento> eventos){
        this.activity = activity;
        this.eventos = eventos;
    }

    @Override
    public int getCount() {
        return eventos.size();
    }

    @Override
    public Object getItem(int position) {
        return eventos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return eventos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View linha = activity.getLayoutInflater().inflate(R.layout.item_lista_eventos, null);

        Evento evento = eventos.get(position);
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        TextView textViewNome = (TextView) linha.findViewById(R.id.text_view_nome);
        textViewNome.setText(evento.getNome());

        TextView textViewData = (TextView) linha.findViewById(R.id.text_view_data);
        String periodo = dateFormat.format(evento.getDataInicio()) + " a "	+ dateFormat.format(evento.getDataFim());
        textViewData.setText(periodo);

        TextView textViewInformacao = (TextView) linha.findViewById(R.id.text_view_informacao);
        textViewInformacao.setText(evento.getInformacoes());

        ImageView imageView = (ImageView) linha.findViewById(R.id.like);
        imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.like));


        return linha;
    }
}
