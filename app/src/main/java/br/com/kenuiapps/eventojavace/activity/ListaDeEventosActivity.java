package br.com.kenuiapps.eventojavace.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import br.com.kenuiapps.eventojavace.R;
import br.com.kenuiapps.eventojavace.adapter.ListaDeEventosAdapter;
import br.com.kenuiapps.eventojavace.dao.EventoDAO;
import br.com.kenuiapps.eventojavace.model.Constantes;
import br.com.kenuiapps.eventojavace.model.Evento;


public class ListaDeEventosActivity extends ActionBarActivity implements DialogInterface.OnClickListener, AdapterView.OnItemClickListener{

    private List<Map<String, Object>> eventosMap;
    private SimpleDateFormat dateFormat;
    private EventoDAO dao;
    private AlertDialog alertDialog;
    private int eventoSelecionado;
    private ListView lista;
    private Evento evento;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listagem_eventos);
        this.alertDialog = criaAlertDialog();
        lista = (ListView) findViewById(R.id.listagem);
        lista.setOnItemClickListener(this);
        carregaLista();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lista_de_eventos, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void carregaLista() {
        dao = new EventoDAO(this);
        List<Evento> eventos = dao.getEventos();
        dao.close();

        ListaDeEventosAdapter adapter = new ListaDeEventosAdapter(this, eventos);
        lista.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_atualizar) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void onClick(DialogInterface dialog, int item) {

        Intent intent;

        switch (item) {
            case 0:
                //intent = new Intent(this, ViagemActivity.class);
                //intent.putExtra(Constantes.VIAGEM_ID, id.toString());
                //startActivity(intent);
                break;

            case 1:
                //intent = new Intent(this, GastoActivity.class);
                //intent.putExtra(Constantes.VIAGEM_ID, id);
                //intent.putExtra(Constantes.VIAGEM_DESTINO, destino);
                //startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, ListaDeIncritosActivity.class);
                intent.putExtra(Constantes.EVENTO_ID, evento.getId());
                startActivity(intent);
                break;

        }
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        evento = (Evento) parent.getItemAtPosition(position);
        alertDialog.show();
    }

    private AlertDialog criaAlertDialog() {
        final CharSequence[] items = {
                getString(R.string.ver_detalhes),
                getString(R.string.palestrantes),
                getString(R.string.lista_de_inscritos)
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.opcoes);
        builder.setItems(items, this);
        return builder.create();
    }

}
