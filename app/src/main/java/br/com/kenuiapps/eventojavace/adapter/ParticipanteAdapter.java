package br.com.kenuiapps.eventojavace.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.kenuiapps.eventojavace.R;
import br.com.kenuiapps.eventojavace.model.Participante;

/**
 * Created by danielbaccin on 18/05/15.
 */
public class ParticipanteAdapter extends BaseAdapter {

    private Activity activity;
    private List<Participante> listaDeParticipantes;

    public ParticipanteAdapter(Activity activity, List<Participante> listaDeParticipantes) {
        this.activity = activity;
        this.listaDeParticipantes = listaDeParticipantes;
    }

    @Override
    public int getCount() {
        return listaDeParticipantes.size();
    }

    @Override
    public Object getItem(int position) {
        return listaDeParticipantes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaDeParticipantes.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View linha = activity.getLayoutInflater().inflate(R.layout.item_lista_participantes, null);

        Participante participante = listaDeParticipantes.get(position);

        ImageView imageView = (ImageView)  linha.findViewById(R.id.icon_participante);
        imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_no_image));

        TextView textViewNome = (TextView) linha.findViewById(R.id.nome_participante);
        textViewNome.setText(participante.getNome());


        return linha;
    }
}
