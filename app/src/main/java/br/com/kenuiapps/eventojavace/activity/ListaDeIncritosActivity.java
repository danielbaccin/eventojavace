package br.com.kenuiapps.eventojavace.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;

import br.com.kenuiapps.eventojavace.R;
import br.com.kenuiapps.eventojavace.fragment.ConsultaParticipanteListaFragment;
import br.com.kenuiapps.eventojavace.model.Constantes;

/**
 * Created by daniel on 18/05/15.
 */
public class ListaDeIncritosActivity extends ActionBarActivity {

    private Long eventoId;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listagem_participante);
        ConsultaParticipanteListaFragment frag = new ConsultaParticipanteListaFragment();

        if (getIntent().hasExtra(Constantes.EVENTO_ID)) {
            eventoId = getIntent().getExtras().getLong(Constantes.EVENTO_ID);
            Bundle bundle = new Bundle();
            bundle.putLong(Constantes.EVENTO_ID, eventoId);
            frag.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, frag)
                    .commit();
        }

    }
}
