package br.com.kenuiapps.eventojavace.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.kenuiapps.eventojavace.helper.DataBaseHelper;
import br.com.kenuiapps.eventojavace.model.Evento;

/**
 * Created by Daniel on 17/05/15.
 */
public class EventoDAO extends DAO {


    public EventoDAO(Context context) {
        super(context);
    }

    public List<Evento> getEventos() {

        Cursor cursor = getDb().query(DataBaseHelper.Evento.TABELA, DataBaseHelper.Evento.COLUNAS,
                                        null, null, null, null, null);
        List<Evento> eventos = new ArrayList<Evento>();
        while(cursor.moveToNext()){
            Evento evento = criarEvento(cursor);
            eventos.add(evento);
        }
        cursor.close();
        return eventos;

    }

    public long inserir(Evento evento){
        ContentValues values = evento.toValues();
        return getDb().insert(DataBaseHelper.Evento.TABELA,null, values);
    }


    private Evento criarEvento(Cursor cursor) {
        Evento evento = new Evento(
                cursor.getLong(cursor.getColumnIndex(DataBaseHelper.Evento.ID)),
                cursor.getLong(cursor.getColumnIndex(DataBaseHelper.Evento.ID_SERVIDOR)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Evento.NOME)),
                new Date(cursor.getLong(cursor.getColumnIndex(DataBaseHelper.Evento.DATA_INICIO))),
                new Date(cursor.getLong(cursor.getColumnIndex(DataBaseHelper.Evento.DATA_FIM))),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Evento.INFORMACOES)),
                cursor.getString(cursor.getColumnIndex(DataBaseHelper.Evento.FEEDBACK))
        );
        return evento;
    }

}
