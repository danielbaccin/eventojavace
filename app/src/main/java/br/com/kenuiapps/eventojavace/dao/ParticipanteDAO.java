package br.com.kenuiapps.eventojavace.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import br.com.kenuiapps.eventojavace.helper.DataBaseHelper;
import br.com.kenuiapps.eventojavace.model.Participante;

/**
 * Created by daniel on 18/05/15.
 */
public class ParticipanteDAO extends DAO{

    public ParticipanteDAO(Context context) {
        super(context);
    }

    public long inserir(Participante participante){
        ContentValues values = participante.toContentValues();
        return getDb().insert(DataBaseHelper.Participante.TABELA,null, values);
    }


    public List<Participante> listaParticipantesDoEvento(Long idEvento) {
        String selection = DataBaseHelper.Participante.ID_EVENTO + " = ?";
        String[] selectionArgs = new String[]{idEvento.toString()};

        Cursor cursor = getDb().query(DataBaseHelper.Participante.TABELA,
                DataBaseHelper.Participante.COLUNAS,
                selection, selectionArgs,
                null, null, null);
        List<Participante> participantes = new ArrayList<Participante>();
        while(cursor.moveToNext()){
            Participante participante = criarParticipante(cursor);
            participantes.add(participante);
        }
        cursor.close();
        return participantes;
    }

    private Participante criarParticipante(Cursor cursor) {
        Participante participante = new Participante(cursor);
        return participante;
    }
}
